package mis.pruebas.apiMongoDB.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mis.pruebas.apiMongoDB.entities.ProductModel;
import mis.pruebas.apiMongoDB.service.ProductService;

@RestController @RequestMapping("api/TechU/v2")
public class ProductController {
	@Autowired
	ProductService productService;
	
		@GetMapping("/productos")
	    public List<ProductModel> getProducts() {
	        return productService.findAll();
	    }

	    @GetMapping("/productos/{id}")
	    public Optional<ProductModel> getProductId(@PathVariable String id){
	        return productService.findById(id);
	    }

	    @PostMapping("/productoN")
	    public ProductModel postProducts(@RequestBody ProductModel newProducto){
	        productService.save(newProducto);
	        return newProducto;
	    }

	    @PutMapping("/productoU")
	    public void putProducts(@RequestBody ProductModel productoToUpdate){
	        productService.save(productoToUpdate);
	    }

	    @DeleteMapping("/productoD")
	    public boolean deleteProducts(@RequestBody ProductModel productoToDelete){
	        return productService.delete(productoToDelete);
	    }
	    
	    @GetMapping("/productoC")
	    public Long countProducts() {
	    	return productService.count();
	    }
	    
	    @DeleteMapping("/productoD/{id}")
	    public boolean deleteById(@PathVariable String id) {
	    	return productService.deleteById(id);
	    }
}