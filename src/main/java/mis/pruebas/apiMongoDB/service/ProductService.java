package mis.pruebas.apiMongoDB.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mis.pruebas.apiMongoDB.entities.ProductModel;

@Service
public class ProductService {
	
	@Autowired
	ProductRepository productRepository;
	
	//Listar todos los elementos.
	public List<ProductModel> findAll() {
		return productRepository.findAll();
	}
	//Listar elemento por ID.
	public Optional<ProductModel> findById(String id) {
		return productRepository.findById(id);
	}
	//Guardar un nuevo elemento.
	public ProductModel save(ProductModel entity) {
		return productRepository.save(entity);
	}
	//Eliminar elemento.
	public boolean delete(ProductModel entity) {
		try { 
			productRepository.delete(entity);
			return true;
		} catch(Exception ex) {
			return false;
		}
	}
	//Contar número de elementos.
	public Long count() {
		return productRepository.count();
	}
	//Eliminar por id.
	public boolean deleteById(String id) {
		ProductModel productDelete = productRepository.findById(id).orElse(null);
		if(productDelete!=null) {
			productRepository.deleteById(id);
			return true;
		}
		return false;
	}
}