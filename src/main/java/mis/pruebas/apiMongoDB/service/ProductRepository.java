package mis.pruebas.apiMongoDB.service;

import org.springframework.data.mongodb.repository.MongoRepository;

import mis.pruebas.apiMongoDB.entities.ProductModel;

public interface ProductRepository extends MongoRepository<ProductModel, String> {

}
