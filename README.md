﻿## Back02 - Integración operaciones CRUD en MongoDB - API REST

En este codelab integraremos Mongo DB para realizar las operaciones CRUD asociadas a las peticiones REST implementadas en los codelabs anteriores.

**URL**: localhost:8700/api/TechU/v2/

## Métodos HTTP 

|                Descripción|Método|URL|
|----------------|-------------------------------|-----------------------------|
|Obtiene todos los elementos de la colección.|GET            |/productos            |
|Obtiene un elemento de la colección mediante el id.          |GET            |/productos/{id}
|Cuenta el número de elementos de la colección.|GET            |/productoC            |
|Añade un nuevo elemento a la colección.|POST            |/productoN
|Actualiza un elemento de la colección.|PUT            |/productoU            |
|Elimina un elemento de la colección.|DELETE            |/productoD            |
|Elimina un elemento de la colección mediante el id.|DELETE            |/productoD/{id}            |
     
Daniel Reyes Castro

